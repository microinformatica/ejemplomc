package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity  implements  View.OnClickListener {

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResul;
    private Button btnSumar,btnRestar,btnMultiplicar,btnDiv, btnLimpiar,btnCerrar;
    private  Operaciones op = new Operaciones();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

     public void initComponts(){
        txtNum1  =  (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (EditText) findViewById(R.id.txtResul);

        btnSumar =  (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar =  (Button) findViewById(R.id.btnCerrar);

        setEventos();

     }
     public void setEventos(){


         this.btnSumar.setOnClickListener(this);
         this.btnRestar.setOnClickListener(this);
         this.btnMultiplicar.setOnClickListener(this);
         this.btnDiv.setOnClickListener(this);
         this.btnLimpiar.setOnClickListener(this);
         this.btnCerrar.setOnClickListener(this);



     }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case 1: // Sumar


        }

    }
}
